class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def welcome_email(user)
    @user = user
    url_token = @user.activation_token
    @url = "localhost:3000/users/#{@user.id}/activate?activation_token=#{url_token}"

    mail(to: user.email, subject: 'Welcome to My Awesome Site')
  end

end
