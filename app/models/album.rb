class Album < ActiveRecord::Base
  attr_accessible :title, :band_id, :album_type

  belongs_to :band,
  :foreign_key => :band_id,
  :primary_key => :id,
  :class_name => "Band"

  has_many :tracks,
  :foreign_key => :album_id,
  :primary_key => :id,
  :class_name => "Track",
  :dependent => :destroy

end
