class User < ActiveRecord::Base
  attr_accessible :email, :password, :session_token, :activation_token, :activated


  before_validation(:ensure_session_token, on: :create)
  before_validation(:ensure_activation_token, on: :create)

  validates :email, :session_token, presence: true
  validates :email, uniqueness: true

  has_many :notes,
  :foreign_key => :user_id,
  :primary_key => :id,
  :class_name => "Note",
  :dependent => :destroy

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)

    return nil if user.nil?

    user.is_password?(password) ? user : nil
  end

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
  end

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end

  def ensure_activation_token
    self.activation_token = self.class.generate_session_token
  end

  def password=(secret)
      write_attribute(:password_digest, BCrypt::Password.create(secret))
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def activate!
    self.activated = true
    self.save
  end

end
