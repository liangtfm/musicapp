class Track < ActiveRecord::Base
  attr_accessible :title, :album_id, :track_type, :lyrics

  belongs_to :album,
  :foreign_key => :album_id,
  :primary_key => :id,
  :class_name => "Album"

  has_many :notes,
  :foreign_key => :track_id,
  :primary_key => :id,
  :class_name => "Note",
  :dependent => :destroy

end
