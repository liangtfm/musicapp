class Band < ActiveRecord::Base
  attr_accessible :name

  has_many :albums,
  :foreign_key => :band_id,
  :primary_key => :id,
  :class_name => "Album",
  :dependent => :destroy

end
