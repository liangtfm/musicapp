module SessionsHelper

  def current_user
    User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    return true unless current_user.nil?
  end

  def log_in_user!
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])

    session[:session_token] = @user.session_token

    redirect_to bands_url
  end

end
