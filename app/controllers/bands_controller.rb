class BandsController < ApplicationController

  def index
    @bands = Band.all
  end

  def show
    @band = Band.find(params[:id])
  end

  def new
    @band = Band.new

    render :new
  end

  def create
    @band = Band.new(params[:band])

    if @band.save
      redirect_to bands_url
    else
      render @band.errors.full_messages, status: :unprocessable_entity
    end
  end

  def edit
    #render :edit ?
    @band = Band.find(params[:id])
  end

  def update
    @band = Band.find(params[:id])

    if @band.update_attributes(params[:band])
      redirect_to bands_url
    else
      render @band.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @band = Band.find(params[:id])

    @band.destroy

    redirect_to bands_url
  end

end
