class TracksController < ApplicationController

  def index
    @tracks = Track.all
  end

  def show
    @track = Track.find(params[:id])
  end

  def new
    @track = Track.new

    render :new
  end

  def create
    @track = Track.new(params[:track])

    if @track.save
      redirect_to albums_url
    else
      render @track.errors.full_messages, status: :unprocessable_entity
    end
  end

  def edit
    #render :edit ?
    @track = Track.find(params[:id])
  end

  def update
    @track = Track.find(params[:id])

    if @track.update_attributes(params[:track])
      redirect_to albums_url
    else
      render @track.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @track = Track.find(params[:id])

    @track.destroy

    redirect_to albums_url
  end

end
