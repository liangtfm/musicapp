class UsersController < ApplicationController

  def new
    @user = User.new

    render :new
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      msg = UserMailer.welcome_email(@user)
      msg.deliver!
      # log_in_user!
      redirect_to bands_url
    else
      render @user.errors.full_messages, status: :unprocessable_entity
    end
  end

  # def activate
  #   @user = User.find(params[:id])
  #
  #   if @user.activation_token == @query
  #     @user.activate!
  #     redirect_to bands_url
  #   else
  #     flash[:errors] = "Not activated!"
  #   end
  # end

end
