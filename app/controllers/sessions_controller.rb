class SessionsController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.find_by_credentials(params[:user][:email], params[:user][:password])

    if @user.nil?
      flash[:errors] = "NO!!!!!!!"
      redirect_to new_session_url
    elsif (@user.activated == false)
      flash[:errors] = "ACTIVATE!!!!"
      redirect_to new_session_url
    else
      @user.reset_session_token!

      session[:session_token] = @user.session_token

      redirect_to bands_url
    end
  end

  def destroy
    session_token = session[:session_token]
    @user = User.find_by_session_token(session_token)

    session[:session_token] = nil
    @user.reset_session_token!

    redirect_to bands_url
  end

end
