class AlbumsController < ApplicationController

  def index
    @albums = Album.all
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
    @album = Album.new

    render :new
  end

  def create
    @album = Album.new(params[:album])

    if @album.save
      redirect_to albums_url
    else
      render @album.errors.full_messages, status: :unproccessable_entity
    end
  end

  def edit
    #render :edit ?
    @album = Album.find(params[:id])
  end

  def update
    @album = Album.find(params[:id])

    if @album.update_attributes(params[:album])
      redirect_to album_url
    else
      render @album.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @album = Album.find(params[:id])

    @album.destroy

    redirect_to bands_url
  end

end
