class NotesController < ApplicationController
  def new
    @note = Note.new

    render partial: 'note'
  end

  def create
    @note = Note.new(params[:note])

    if @note.save
      redirect_to track_url(@note.track_id)
    else
      render @note.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @note = Note.find(params[:id])

    @note.destroy

    redirect_to track_url(@note.track_id)
  end

  def edit
    @note = Note.find(params[:id])
  end

  def update
    @note = Note.find(params[:id])

    if @note.update_attributes(params[:note])
      redirect_to track_url
    else
      render @note.errors.full_messages, status: :unprocessable_entity
    end
  end

end
